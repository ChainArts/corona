import { Chart } from 'chart.js'
import { parse } from 'papaparse'

document.getElementById("avglen").addEventListener('input', (e) => {
  changeAverage(parseInt((<HTMLInputElement>e.target).value))
})

let infections: Array<number>;
let chart1: Chart;
let chart2: Chart;

function calculateAverage(data: Array<number>, lenOfAvg: number): Array<number> {
  let ret = []
  for(let i = 0; i < data.length; i++)   {
     let values = data.slice(i - Math.ceil(lenOfAvg / 2), i + Math.floor(lenOfAvg / 2))
     let avg = values.reduce((a,b)=> a + b, 0) / lenOfAvg
     ret.push(avg)
  }

  ret = ret.slice(0, ret.length - Math.floor(lenOfAvg / 2))

  return ret
}

function calculateDerivative(data: Array<number>): Array<number> {
  let ret = []
  
  for(let i = 0; i < data.length -1; i++){
    ret.push(data[i] - data[i-1]) 
  }

  return ret
}

function calculateIntegral(data: Array<number>): Array<number> {
  let ret = [0]
  
  for(let i = 0; i < data.length -1; i++){
    ret.push(data[i] + ret[i]) 
  }

  return ret
}

function changeAverage(days: number) {
  const avg = calculateAverage(infections, days)
  chart1.data.datasets[1].data = avg
  chart1.data.datasets[1].label = days + ' Tage Mittelwert'
  chart1.update()
}

async function main() {
	const epidata_raw = await (await fetch("./epi.csv")).text()
	const allgemeindata_raw = await (await fetch("./allgemein.csv")).text()
	const genesene_raw = await (await fetch("./genesene.csv")).text()
  const genesene = <Array<Array<string>>> parse(genesene_raw).data
  const epidata = <Array<Array<string>>> parse(epidata_raw).data
  const allgemeindata = <Array<Array<string>>> parse(allgemeindata_raw).data
  document.getElementById('curSick').innerText = allgemeindata[1][0];
  document.getElementById('positiv').innerText = allgemeindata[1][1];
  document.getElementById('cured').innerText = allgemeindata[1][2];
  document.getElementById('dead').innerText = allgemeindata[1][3];
  document.getElementById('deathRate').innerText = 
    (Math.round(
      100000 *  parseInt(allgemeindata[1][3]) / parseInt(allgemeindata[1][1])
  ) / 1000) + "%";
  document.getElementById('dataDate').innerText = allgemeindata[1][13];
    
  epidata.shift()
  epidata.pop()
  const labels = epidata.map((data) => data[0])
  infections = epidata.map((data) => parseInt(data[1]))

  genesene.shift()
  const genesene_data = genesene.map((data) => parseInt(data[1]))

  const runningAvg = calculateAverage(infections, 14)

	const ctx  = <HTMLCanvasElement> document.getElementById('chart1')
	chart1 = new Chart(ctx, {
			type: 'line',
			data: {
          labels,
					datasets: [{
							label: 'tägliche Infektionen',
              data: infections,
              borderColor: 'rgb(255, 0, 0)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
              label: 14 + ' Tage Mittelwert' ,
              data: runningAvg,
              borderColor: 'rgb(0, 0, 255)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
              label: 'Genesungen/Tag',
              data: calculateDerivative(genesene_data),
              borderColor: 'rgb(0, 255, 0)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
          }]
			},
			options: {
          maintainAspectRatio: false,
					scales: {
							yAxes: [{
                  id: 'A',
                  position: 'left',
									ticks: {
											beginAtZero: true
									}
              }]
					}
			}
	});
  
	const ctx2  = <HTMLCanvasElement> document.getElementById('chart2')
	chart2 = new Chart(ctx2, {
			type: 'line',
			data: {
          labels,
					datasets: [{
							label: 'Fälle',
              data: calculateIntegral(infections),
              borderColor: 'rgb(255, 0, 0)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}, {
							label: 'Genesen',
              data: genesene_data,
              borderColor: 'rgb(0, 0, 255)',
              borderWidth: 2,
              fill: false,
              pointBorderWidth: 1,
              pointRadius: 2,
					}]
			},
			options: {
          maintainAspectRatio: false,
					scales: {
							yAxes: [{
                  id: 'A',
                  position: 'left',
									ticks: {
											beginAtZero: true
									}
              }]
					}
			}
	});
}

var checkbox = document.querySelector('input[name=theme]')
checkbox.addEventListener('change', function(){
    if(this.checked) {
        trans()
        document.documentElement.setAttribute('data-theme', 'dark')
    } else {
        trans()
        document.documentElement.setAttribute('data-theme', 'light')
    }
})

let trans = () => {
    document.documentElement.classList.add('transition');
        window.setTimeout(() => {
            document.documentElement.classList.remove('transition')
        }, 500)
    }
main();
