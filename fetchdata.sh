#!/bin/bash

curl https://info.gesundheitsministerium.at/data/Epikurve.csv > public/epi.csv
curl https://info.gesundheitsministerium.at/data/AllgemeinDaten.csv > public/allgemein.csv
curl https://info.gesundheitsministerium.at/data/GenesenTimeline.csv > public/genesene.csv
